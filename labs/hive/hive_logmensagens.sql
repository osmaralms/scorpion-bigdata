
create database meubd;

create table meubd.log_mensagens (text string) partitioned by(ano string, mes string, dia string);

set hive.exec.dynamic.partition.mode=nonstrict;

insert into meubd.log_mensagens partition(ano, mes, dia) values ('log text 20191101', '2019', '11', '01');
insert into meubd.log_mensagens partition(ano, mes, dia) values ('log text 20191102', '2019', '11', '02');
insert into meubd.log_mensagens partition(ano, mes, dia) values ('log text 20191103', '2019', '11', '03');